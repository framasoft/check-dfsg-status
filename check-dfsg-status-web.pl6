#!/usr/bin/env perl6
use v6;

use strict;

use lib 'P6-Net-IMAP/lib/';
use Template::Mojo;
use File::Directory::Tree;
use Net::IMAP;
use Terminal::Spinners;
use JSON::Fast;

grammar Cds {
    token TOP       {
        <nfsection>*\n*
        <csection>*\n*
        .*
    }
    token nfsection {
        <nonfree>\n+
        <packages>+
    }
    token csection  {
        <contrib>\n+
        <packages>+
    }
    token nonfree   {
        ^^ \s+ 'Non-free packages installed on ' $<server>=[\N+]
    }
    token contrib   {
        ^^ \s+ 'Contrib packages installed on ' $<server>=[\N+]
    }
    token packages  {
        ^^ $<package>=[\S+]\s+\N*\n
    }
}

class Server {
    has Str $.name;
    has Str @.nonfree;
    has Str @.contrib;
}

my Str $user   = %*ENV<USER>;
my Str $pwd    = %*ENV<PASSWORD>;
my Str $server = %*ENV<SERVER>;
my Str $theme  = %*ENV<THEME> || 'default';
my (@servers, %nonfree, %contrib);

unless ($user && $pwd && $server) {
    say "Please set USER env variable"     unless $user;
    say "Please set PASSWORD env variable" unless $pwd;
    say "Please set SERVER env variable"   unless $server;
    say "Exiting";
    exit;
}

my $theme_dir = "themes".IO.add($theme);

# Connect to IMAP server
my Net::IMAP::Simple $i = Net::IMAP.new(:$server);
$i.authenticate($user, $pwd);
$i.select('INBOX');

# Fetch messages
my @messages;
if (%*ENV<DEV> ) {
    @messages = $i.search(:all);
} else {
    @messages = $i.search(:unseen);
}

unless @messages.elems {
    say "No new messages.";
    exit;
}

print "{@messages.elems} messages to process…  ";

# Parse messages
my Spinner $spinner .= new: type => 'dots';
for @messages -> $msg {
    $spinner.next;
    my ($name, @nonfree, @contrib);

    my $body   = $msg.mime.body-str;

    # Get datas
    if $body ~~ /.* 'No non-free or contrib packages installed on ' (\N+) '!  rms would be proud.' .*/ {
        $name = $0.Str;
    } elsif $body ~~ /.* 'No non-free or contrib packages installed on ' (\N+) '!  You have completed the first step to enlightenment.' .*/ {
        $name = $0.Str;
    } else {
        my $m = Cds.parse($body);

        for $m<nfsection> -> $q {
            $name = $q<nonfree><server>.Str;

            for $q<packages> -> $p {
                @nonfree.push($p<package>.Str);
                %nonfree{$p<package>.Str} += 1;
            }
        }
        for $m<csection> -> $q {
            $name ||= $q<contrib><server>.Str if $q<contrib><server>;

            for $q<packages> -> $p {
                @contrib.push($p<package>.Str);
                %contrib{$p<package>.Str} += 1;
            }
        }
    }

    @servers.push(Server.new(:$name, :@nonfree, :@contrib));
}

print "
Messages processed.
Generating HTML and JSON for {@servers.elems} servers …  ";

# Choose template
$spinner .= new: type => 'dots';
@servers .= sort: *.name;
%nonfree .= sort: *.key;
%contrib .= sort: *.key;

my %temp = ( free => 0, nonfree => 0, contrib => 0 );
my @servers_tbody;
eager @servers.map: {
    $spinner.next;
    @servers_tbody.push([
        (%*ENV<HIDENAMES>) ?? '[Filtered]' !! $_.name,
        ($_.nonfree.elems) ?? "({$_.nonfree.elems}) $_.nonfree.join(' ')" !! "∅",
        ($_.contrib.elems) ?? "({$_.contrib.elems}) $_.contrib.join(' ')" !! "∅"
    ]);
    if ($_.nonfree.elems + $_.contrib.elems == 0) {
        %temp<free>++;
    } elsif ($_.nonfree.elems !== 0) {
        %temp<nonfree>++;
    } else {
        %temp<contrib>++;
    }
};
my %servers_freedom = (
    label => "Servers freedom",
    datasets => [{
        data => [ %temp<free>, %temp<nonfree>, %temp<contrib> ]
    }],
    "labels" => [ "with only free packages", "with nonfree packages (and maybe contrib)", "with contrib packages (and no nonfree)" ]
);

my %nonfree_use =  (
    label => "Non-free packages use",
    datasets => [{
        data => []
    }],
    "labels" => []
);
my @nonfree_tbody;
for %nonfree.kv -> $k, $v {
    @nonfree_tbody.push([$k, $v]);
    %nonfree_use<datasets>[0]<data>.push($v);
    %nonfree_use<labels>.push($k);
}

my %contrib_use =  (
    label => "Contrib packages use",
    datasets => [{
        data => []
    }],
    "labels" => []
);
my @contrib_tbody;
for %contrib.kv -> $k, $v {
    @contrib_tbody.push([$k, $v]);
    %contrib_use<datasets>[0]<data>.push($v);
    %contrib_use<labels>.push($k);
}

# Render the datas
my %params = (
    servers => @servers_tbody,
    nonfree => %nonfree,
    contrib => %contrib
);
my Template::Mojo $tpl .= new($theme_dir.add("index.html.ep").slurp);
$theme_dir.add("front/public/index.html").spurt($tpl.render(%params));


$theme_dir.add("front/public/data.json").spurt(to-json({
    servers => %servers_freedom,
    nonfree => %nonfree_use,
    contrib => %contrib_use
}));

$theme_dir.add("front/public/tabs.json").spurt(to-json([
    {
        name     => "Charts",
        table    => False,
        selected => True
    },
    {
        name     => "By server",
        thead    => ["Server", "Non-free packages", "Contrib packages"],
        tbody    => @servers_tbody
    },
    {
        name     => "Non-free packages",
        thead    => ["Packages", "Occurrence (for {@servers.elems} servers)"],
        tbody    => @nonfree_tbody
    },
    {
        name     => "Contrib packages",
        thead    => ["Packages", "Occurrence (for {@servers.elems} servers)"],
        tbody    => @contrib_tbody
    }
]));

say "
HTML and JSON generation successfully ended.
Building frontend…";

indir($theme_dir.add("front"), {
    my $proc = run <yarn build>, :out, :err;
    $proc.out.slurp(:close).say;
    $proc.err.slurp(:close).say;

    say "Frontend built."
})
